from django.db import models

class Restaurants(models.Model):
    name = models.CharField(max_length=40)
    address = models.CharField(max_length=100)
    phone_number = models.CharField(max_length=20)
    created_at = models.DateField(auto_now_add=True)
    updated_at = models.DateField(auto_now=True)

class Bracket(models.Model):
    name = models.CharField(max_length=100)
    rounds_total = models.IntegerField()
    created_at = models.DateField(auto_now_add=True)
    updated_at = models.DateField(auto_now=True)

class Competition(models.Model):
    round_number = models.IntegerField()
    restaurant_a = models.ForeignKey(Restaurants, related_name='restaurant_a', on_delete=models.CASCADE)
    restaurant_b = models.ForeignKey(Restaurants, related_name='restaurant_b', on_delete=models.CASCADE)
    bracket = models.ForeignKey(Bracket, on_delete=models.CASCADE)
    winner = models.ForeignKey(Restaurants, related_name='winner', on_delete=models.CASCADE, null=True)
    # next_competition = models.id
    created_at = models.DateField(auto_now_add=True)
    updated_at = models.DateField(auto_now=True)
