from django.db import models
from datetime import datetime
from app.models import Bracket
from app.models import Competition
from app.models import Restaurants
"""
- when run, this function will insert rows into the bracket and competition tables of the db
- Input: list of restaurants. If restaurants not present, this will fail
- Input: user needs to provide a bracket name
- side effects: write a single row into Bracket based on input into Restaurants
- side effects: write rows into Competition based on input from read
- Output: our newly created Bracket object
- def make_bracket():
"""

"""
11/4 update - will want to make the Restaurants function first, _then_ makebracket. Compeition requires a list of restaurants, the only way to get this list is by having the list available - preiously you/I had tried to do this out of order
"""


def makebracket():
    restaurant1 = Restaurants.objects.create(name='Taqueria Los Comales',address='1544 W 18th St Chicago, IL 60608',phone_number='(312)666-2251')
    restaurant2 = Restaurants.objects.create(name='Bobijoa Korean Kitchen',address='1140 W 18th St Chicago, IL 60608',phone_number='(312)877-5333')
    bracket = Bracket.objects.create(name='test bracket 4',rounds_total=16)
    competition = Competition.objects.create(round_number='1',restaurant_a=restaurant1,restaurant_b=restaurant2,bracket=bracket)
    return bracket



"""
to-do
accept a list of restaurants (loops through list for matchups)
add randomization to restaurant matchups
tests!
"""


#Long term: have actual inputs in a UI
